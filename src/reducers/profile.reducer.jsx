const profiletState = [
    {id:1, name:'Vasya', surname:'Edkii', creditcard: '1488'},
    {id:2, name:'Jora', surname:'Jvachka', creditcard: '1337'},
    {id:3, name:'Sosa', surname:'Losa', creditcard: '7788'},
];

export default (state = profiletState, action) => {
    switch (action.type) {
        case 'ADD_PROFILE':
            return [...state, action.data];
        case 'DELETE_PROFILE':
            return state.filter((el) => el.id !== action.id)
        case 'EDIT_PROFILE':
            let newState = [];
            state.forEach((el) => {
                if(el.id === action.data.id) {
                    newState.push(action.data)
                } else {
                    newState.push(el)
                }});
            return newState;
        default:
            return state;
    }
};
