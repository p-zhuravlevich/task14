const defaultState = [
    {id:1, name:'Myaso', image:'https://pngimg.com/uploads/meat/small/meat_PNG98294.png', description: 'DA ETO JE KUSOK MYASA! DO YOU LOVE MYASO?? OF COURE U LOVE MYASO!', price: '555'},
    {id:2, name:'Pizza', image:'https://www.pngall.com/wp-content/uploads/2016/05/Pizza-Download-PNG.png', description: 'GREATEST PIZZA EVER!!!! SMACHNO!', price: '555'},
    {id:3, name:'Cheburek', image:'https://i3.stat01.com/1/8962/89618237/795f32/cheburek.png', description: 'THIS IS CHEBUREK!! DO YOU WANT CHEBUREK???', price: '555'},
];

export default (state = defaultState, action) => {
    switch (action.type) {
        case 'ADD_PRODUCT':
            return [...state, action.data];
        case 'DELETE_PRODUCT':
            return state.filter((el) => el.id !== action.id)
        case 'EDIT_PRODUCT':
            let newState = [];
            state.forEach((el) => {
                if(el.id === action.data.id) {
                    newState.push(action.data)
                } else {
                    newState.push(el)
                }});
            return newState;
        default:
            return state;
    }
};
