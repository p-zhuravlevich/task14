import './App.css';
import React from 'react';
import {Provider} from 'react-redux';
import {store} from "./products/products";
import Navigation from './Navigation';
import Home from './components/Home';
import Admin from './components/Admin';
import User from './components/User';
import Profile from './components/Profile';
import {
  BrowserRouter as Router,
  Route,
} from "react-router-dom";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Router>
          <Navigation />
            <Provider store={store}>
              <Route exact path="/" component={Home} />
              <Route path="/admin" component={Admin} />
              <Route path="/user" component={User} />
              <Route path="/profile" component={Profile} />
            </Provider>
        </Router>
      </header>
    </div>
  );
}

export default App;
