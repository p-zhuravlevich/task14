import React, {useState} from 'react';
import {connect} from "react-redux";

function User(props){
    const [name, setName] = useState();
    const [selectedName, setSelectedName] = useState();

    const findValueProduct = () =>{
        setSelectedName(name)
    }

return ( 
    <>
        <div><h1>Найти товар</h1></div>
        <div class="input__form">
            <span>Найти: </span> <input value={name} onChange={(e) => setName(e.target.value)}/>
        <button onClick={findValueProduct}>Отправить</button>
        </div>
        <div className="product__block__parent">
        {
                props.products.filter((product) => {
                    if (selectedName) {
                    return product.name.toLowerCase() === selectedName.toLowerCase()
                    } return true
                }).map(product => (
                        <div key={product.id} className="product__block">
                            <div style={{fontSize:22}}>
                                <h3>{product.name}</h3> 
                                <img src={product.image} /> 
                                <p>{product.description}</p> 
                                <p>{product.price}</p></div>
                        </div>
                    ))
                }
        </div>
    </>)
}

const mapStateToProps = (state) => ({
    products: state.products,
});

export default connect(
    mapStateToProps,
)(User);