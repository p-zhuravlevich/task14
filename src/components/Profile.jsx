import React, {useEffect, useState} from 'react';
import {connect} from "react-redux";

function Profile(props) {
    const [name, setName] = useState();
    const [surname, setSurname] = useState();
    const [creditcard, setCreditcard] = useState();
    const [id, setId] = useState();
    const [isEdit, setIsEdit] = useState(false);

    useEffect(() => {
    }, [name,surname,creditcard])

    const sendNewProfile = () => {
        let profile = {
            id: isEdit ? id : props.profiles.length+1,
            name: name,
            surname: surname,
            creditcard: creditcard,
        };

        if(isEdit) {
            props.editProfile(profile);
        } else {
            props.addProfile(profile);
        }

        setName('');
        setSurname('');
        setCreditcard('');
        setIsEdit(false);
    }

    const handleEdit = (profile) => {
        setIsEdit(true);
        setId(profile.id);
        setName(profile.name);
        setSurname(profile.surname);
        setCreditcard(profile.creditcard);
    }

    const handleDelete = (profile) => {
        props.deleteProfile(profile.id)
    }

        console.log(props.profiles)
        return (
            <>
            <div><h1>Создать профиль</h1></div>
            <div class="input__form">
            <span>Имя: </span> <input value={name} onChange={(e) => setName(e.target.value)}/>
            <span>Фамилия: </span> <input value={surname} onChange={(e) => setSurname(e.target.value)}/>
            <span>Карта: </span> <input value={creditcard} onChange={(e) => setCreditcard(e.target.value)}/>
            <button onClick={sendNewProfile}>Отправить</button>
            </div>
            <div className="profile__block__parent">
                {
                    props.profiles.map(profile => (
                        <div key={profile.id} className="product__block">
                            <div style={{fontSize:22}}>
                                <p>Имя: <strong>{profile.name}</strong></p> 
                                <p>Фамилия: <strong>{profile.surname}</strong></p>
                                <p>Карта: <strong>{profile.creditcard}</strong></p></div>
                            <button onClick={() => handleEdit(profile)}>Изменить</button>
                            <button onClick={() => handleDelete(profile)}>Удалить</button>
                        </div>
                    ))
                }
            </div>
                </>
        )
}

const mapStateToProps = (state) => ({
    profiles: state.profiles
});

const mapDispatchToProps = (dispatch) => ({
    addProfile: (data) => {dispatch({type: 'ADD_PROFILE', data})},
    editProfile: (data) => {dispatch({type: 'EDIT_PROFILE', data})},
    deleteProfile: (id) => {dispatch({type: 'DELETE_PROFILE', id})},
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Profile);
