import React, {useEffect, useState} from 'react';
import {connect} from "react-redux";

function Admin(props) {
    const [name, setName] = useState();
    const [description, setDescription] = useState();
    const [image, setImage] = useState();
    const [price, setPrice] = useState();
    const [id, setId] = useState();
    const [isEdit, setIsEdit] = useState(false);

    useEffect(() => {
    }, [name,description,image,price])

    const sendNewProduct = () => {
        let product = {
            id: isEdit ? id : props.products.length+1,
            name: name,
            description: description,
            image: image,
            price: price,
        };

        if(isEdit) {
            props.editProduct(product);
        } else {
            props.addProduct(product);
        }

        setName('');
        setDescription('');
        setImage('');
        setPrice('');
        setIsEdit(false);
    }

    const handleEdit = (product) => {
        setIsEdit(true);
        setId(product.id);
        setName(product.name);
        setDescription(product.description);
        setImage(product.image);
        setPrice(product.price);
    }

    const handleDelete = (product) => {
        props.deleteProduct(product.id)
    }

        console.log(props.products)
        return (
            <>
            <div><h1>Создать товар</h1></div>
            <div class="input__form">
            <span>Название: </span> <input value={name} onChange={(e) => setName(e.target.value)}/>
            <span>Изображение: </span> <input value={image} onChange={(e) => setImage(e.target.value)}/>
            <span>Описание: </span> <input value={description} onChange={(e) => setDescription(e.target.value)}/>
            <span>Цена: </span> <input value={price} onChange={(e) => setPrice(e.target.value)}/>
            <button onClick={sendNewProduct}>Отправить</button>
            </div>
            <div className="product__block__parent">
                {
                    props.products.map(product => (
                        <div key={product.id} className="product__block">
                            <div style={{fontSize:22}}>
                                <h3>{product.name}</h3> 
                                <img src={product.image} /> 
                                <p>{product.description}</p>
                                <p>{product.price}</p></div>
                            <button onClick={() => handleEdit(product)}>Изменить</button>
                            <button onClick={() => handleDelete(product)}>Удалить</button>
                        </div>
                    ))
                }
            </div>
            </>
        )
}

const mapStateToProps = (state) => ({
    products: state.products
});

const mapDispatchToProps = (dispatch) => ({
    addProduct: (data) => {dispatch({type: 'ADD_PRODUCT', data})},
    editProduct: (data) => {dispatch({type: 'EDIT_PRODUCT', data})},
    deleteProduct: (id) => {dispatch({type: 'DELETE_PRODUCT', id})},
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Admin);
