import {createStore, combineReducers} from "redux";
import productReducer from '../reducers/product.reducer';
import profileReducer from '../reducers/profile.reducer';

export const rootReducer = combineReducers({
    products: productReducer,
    profiles: profileReducer
});

export const store = createStore(rootReducer);
