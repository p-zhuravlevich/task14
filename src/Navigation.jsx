import React from "react";
import {NavLink} from "react-router-dom";

export default function Navigation(){
    return(
        // <Router>
            <>
                <nav>
                    <ul>
                        <li><NavLink to="/">Home</NavLink></li>
                        <li><NavLink to="/admin">Admin Part</NavLink></li>
                        <li><NavLink to="/user">User Part</NavLink></li>
                        <li><NavLink to="/profile">Profile</NavLink></li>
                    </ul>
                </nav>
            </>
    // </Router>
    )
}

